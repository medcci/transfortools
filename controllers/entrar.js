module.exports = function(app) {
    var controller = {},
        User = app.models.users,
        bcrypt = require('bcrypt-nodejs');

    controller.index = function(req, res) {
        res.render("entrar/index");
    };

    controller.checkAuth = function(req, res, next) {
        if (!req.session.user_id) {
            req.flash("info", "Você precisa esta logado para acessar essa página");
            res.redirect("/entrar");
        }
        else {
            next();
        }
    };

    controller.checkAdmin = function(req, res, next) {
        if (!req.session.isAdmin) {
            res.render("error/auth");
        }
        else {
            next();
        }
    };

    controller.login = function(req, res) {
        var // Usuario e Senha;
            uname = req.body.username.charAt(0).toLowerCase() + req.body.username.slice(1),
            psswd = req.body.password;

        User.findOne({
            "username": uname
        }, function(error, data) {
            if (!data) {
                req.flash("exit", "Senha ou nome de usuario não foram encontrados!");
                res.redirect("/entrar");
            }
            else {
                bcrypt.compare(psswd, data.password, function(err, password) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        if (error) {
                            console.log(error);
                        }
                        else if (!data) {
                            req.flash('exit', 'Usúario não cadastrado');
                            res.redirect('/entrar');
                        }
                        else {

                            if (uname === data.username && password === true) {
                                req.session.user_id = data._id;
                                if (data.isAdmin === true) {
                                    req.session.isAdmin = data._id;
                                }
                                res.redirect("/");
                            }
                            else {
                                req.flash("exit", "Senha ou nome de usuario não foram encontrados!");
                                res.redirect("/entrar");
                            }
                        }
                    }
                });
            }
        });

    };

    controller.logout = function(req, res) {
        delete req.session.user_id;
        delete req.session.isAdmin;
        req.flash("exit", "Você saiu da conta!");
        res.redirect("/entrar");
    };

    return controller;
};