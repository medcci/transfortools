module.exports = function(app) {
    var controller = {};
    var request = require("request");
    
    controller.index = function(req, res) {
        request({
            url: "http://api.micetigri.fr/json/mapcrew",
            json: true
        }, function(error, response, body) {
            if (!error && response.statusCode === 200) {
                res.render("mapcrews/index", {
                    data: body,
                    isLogged: req.session.user_id
                });
            }
        });
    };

    return controller;
};