module.exports = function(app) {
    var controller = {},
        User = app.models.users,
        Verificar = app.models.verificar;

    controller.index = function(req, res) {
        User.find(function(error, data) {
            if (error) {
                console.log(error);
            }
            else {
                Verificar.find(function(error, verificars) {
                    res.render("painel/index", {
                        lista: data,
                        veriLista: verificars
                    });
                });
            }
        });
    };

    controller.editar = function(req, res) {
        User.findById(req.params.id, function(error, data) {
            if (error) {
                console.log(error);
            }
            else {
                res.render("painel/editar", {
                    editar: data
                });
            }
        });
    };

    controller.update = function(req, res) {
        User.findById(req.params.id, function(error, data) {
            if (error) {
                console.log(error);
            }
            else {
                var model = data;
                model.username = req.body.username;
                model.tfmuser = req.body.tfmuser;
                model.isValid = req.body.isValid;
                model.isAdmin = req.body.isAdmin;

                model.save(function(error) {
                    if (error) {
                        console.log(error);
                    }
                    else {
                        req.flash("info", "Usúario atualizado com sucesso!");
                        res.redirect("/painel/editar/" + req.params.id);
                    }
                });
            }
        });
    };

    controller.deletar = function(req, res) {
        User.remove({
            _id: req.params.id
        }, function(err) {
            if (err) {
                console.log(err);
            }
            else {
                req.flash('info', 'Usuário excluído com sucesso!');
                res.redirect('/painel');
            }
        });
    };
    
    controller.verificar = function(req, res) {
        Verificar.remove({
            id: req.params.id
        }, function(err) {
            if (err) {
                console.log(err);
            }
            else {
                res.redirect('/painel/editar/'+req.params.id);
            }
        });
    };


    return controller;
};