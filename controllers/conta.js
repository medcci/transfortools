module.exports = function(app) {
    var controller = {},
        User = app.models.users,
        Verificar = app.models.verificar,
        bcrypt = require('bcrypt-nodejs');


    controller.index = function(req, res) {
        User.findById(req.session.user_id, function(error, data) {
            if (error) {

            }
            else {
                res.render("conta/index", {
                    data: data
                });
            }
        });
    };

    controller.verificar = function(req, res) {
        User.findById(req.params.id, function(error, data) {
            if (!data) {
                res.redirect("/conta");
            }
            else {
                var model = new Verificar();
                model.username = data.username;
                model.id = data._id;
                model.save(function(error) {
                    if (error) {
                        req.flash("info", "Você já esta na lista de espera");
                        res.redirect("/conta");
                    }
                    else {
                        req.flash("info", "Você sera verificado em breve");
                        res.redirect("/conta");
                    }
                });
            }
        });
    };

    controller.senha = function(req, res) {
        User.findById(req.params.id, function(error, data) {
            if (!data && error) {
                res.redirect("/conta");
            }
            else {
                bcrypt.compare(req.body.password, data.password, function(err, password) {
                    if (err) {
                        console.log(err);
                    }
                    else {
                        if (password === true) {
                            var model = data;
                            model.username = data.username;
                            model.tfmuser = data.tfmuser;
                            model.password = req.body.newPassword;

                            model.save(function(error) {
                                if (error) {
                                    req.flash("info", "Não foi possivel alterar sua senha");
                                    res.redirect("/conta");
                                }
                                else {
                                    delete req.session.user_id;
                                    req.flash("exit", "Senha alterada com successo, faça o login novamente");
                                    res.redirect("/entrar");
                                }
                            });
                        }
                        else {
                            req.flash("info", "Não foi possivel alterar sua senha");
                            res.redirect("/conta");

                        }
                    }
                });
            }
        });
    };

    return controller;
};