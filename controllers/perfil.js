module.exports = function(app) {
    var controller = {},
        request = require("request"),
        User = app.models.users;
    controller.getName = function(req, res) {
        var name = encodeURIComponent(req.body.nome);
        res.redirect("/perfil/" + name);
    };

    controller.perfil = function(req, res) {
        var cheeseFormice;

        request({
            url: "http://api.formice.com/mouse/stats.json?n=" + encodeURIComponent(req.params.nome) + "&l=br",
            json: true
        }, function(error, response, body) {
            if (!error && response.statusCode === 200) {
                cheeseFormice = body;
            }
        });

        request({
            url: "http://api.micetigri.fr/json/player/" + encodeURIComponent(req.params.nome),
            json: true
        }, function(error, response, body) {
            if (!error && response.statusCode === 200) {
                User.findOne({
                    'tfmuser': req.params.nome.toLowerCase()
                }, function(error, user) {
                    if (error) {
                        console.log(error);
                    }
                    else {
                        res.render("perfil/index", {
                            data: body,
                            cheeseFormice: cheeseFormice,
                            user: user
                        });
                    }
                });
            }
            else {
                req.flash("info", "Jogador não encontrado, por favor procurar novamente!");
                res.redirect("/procurar-jogador");
            }
        });
    };

    return controller;
};