module.exports = function(app) {
    var controller = {},
        User = app.models.users;
    
    controller.index = function(req, res) {
        res.render("registrar/index");
    };
    
    controller.insert = function(req, res){
        var userRegistrar = new User(req.body);
        
        userRegistrar.save(function(error) {
            if (error) {
                req.flash("danger", "Este perfil ou nome de usuário já se encontra registrado, tente novamente");
                res.redirect("/registrar");
            }else{
                req.flash("info", "Você foi registrado com sucesso");
                res.redirect("/entrar");
            }
        });
    };
    
    return controller;
};