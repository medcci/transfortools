module.exports = function(app) {
    var controller = {};
    var request = require("request");

    controller.index = function(req, res) {
        request({
            url: "http://api.micetigri.fr/json/sentinels",
            json: true
        }, function(error, response, body) {
            res.render("sentinelas/index", {
                data: body
            });
        });
    };

    return controller;
};
