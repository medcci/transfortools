module.exports = function(app) {
    var controller = {};
    
    controller.index = function(req, res) {
        res.render("main/index");
    };
    
    return controller;
};