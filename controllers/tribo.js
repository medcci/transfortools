module.exports = function(app) {
    var controller = {},
        Base64 = require("js-base64").Base64,
        request = require("request");

    controller.getName = function(req, res) {
        var name = encodeURIComponent(req.body.tribo);
        res.redirect("/tribo/" + name);
    };
    
    controller.index = function(req, res) {
        request({
            url: "http://api.formice.com/tribe/stats.json?t=" + Base64.encode(req.params.nome),
            json: true
        }, function(error, response, body) {
            if (!error && response.statusCode === 200 && !body.error) {
                res.render("tribo/index", {
                    data: body
                });
            }
            else {
                req.flash("info", "Tribo não encontrada, por favor procurar outra tribo!");
                res.redirect("/procurar-tribo");
            }
        });
    };

    return controller;
};