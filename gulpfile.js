var gulp = require('gulp');
var jshint = require("gulp-jshint");

gulp.task('jshint', function() {
    return gulp.src(['./controllers/*.js', './middleware/*.js', './models/*.js',
    './routes/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('default', ['jshint']);