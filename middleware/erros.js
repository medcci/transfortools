exports.notfound = function(req, res, next) {
    res.status(404);
    res.render('error/404');
};
exports.serverError = function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error/500', {
        message: console.log(err.message),
        error: {},
    });
};
