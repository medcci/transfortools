module.exports = function(app) {
    var entrar = app.controllers.entrar;
    
    app.get("/entrar", entrar.index);
    app.post("/entrar", entrar.login);
    app.get("/sair", entrar.logout);
};