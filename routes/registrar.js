module.exports = function(app) {
    var registrar = app.controllers.registrar;
    
    app.get("/registrar", registrar.index);
    app.post("/registrar", registrar.insert);
};