module.exports = function(app) {
    var perfil = app.controllers.perfil;
    
    app.get("/perfil/:nome", perfil.perfil);
    app.post("/perfil", perfil.getName);
    
    app.get("/procurar-jogador", function(req, res) {
        res.render("perfil/procurar");
    });
};