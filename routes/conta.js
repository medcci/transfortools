module.exports = function(app) {
    var check = app.controllers.entrar,
        conta = app.controllers.conta,
        User = app.models.users;
      
    app.get("/conta", check.checkAuth, conta.index);
    app.get("/conta/verificar/:id", check.checkAuth, conta.verificar);
    app.get("/conta/senha/:id",check.checkAuth, function(req, res) {
        User.findById(req.params.id, function(error, data){
            if(!data) {
                res.redirect("/conta");
            }else{
                res.render("conta/senha", {userData: data});
            }
        });
    });
    app.post("/conta/senha/:id",check.checkAuth, conta.senha);
};