module.exports = function(app) {
    var check = app.controllers.entrar,
        painel = app.controllers.painel;
      
    app.get("/painel", check.checkAdmin, painel.index);
    app.get("/painel/editar/:id", check.checkAdmin, painel.editar);
    app.post("/painel/editar/:id", check.checkAdmin, painel.update);
    app.get("/painel/verificar/:id", check.checkAdmin, painel.verificar);
    app.get("/painel/deletar/:id", check.checkAdmin, painel.deletar);
};