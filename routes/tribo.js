module.exports = function(app) {
  var tribo = app.controllers.tribo;

  app.get("/tribo/:nome", tribo.index);
  app.post("/tribo", tribo.getName);
  app.get("/procurar-tribo", function(req, res) {
    res.render("tribo/procurar");
  });
};