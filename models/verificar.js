module.exports = function() {
    var mongoose = require("mongoose"),
        Schema = mongoose.Schema;

    var Verificar = new Schema({
        username: {
            type: String,
            unique: true,
            lowercase: true
        },
        pedido: {
            type: Boolean,
            default: true
        },
        id: {
            type: String,
            unique: true
        }
    });
    
    return mongoose.model('Verificar', Verificar);
};