module.exports = function() {
    var mongoose = require("mongoose"),
        bcrypt = require('bcrypt-nodejs'),
        Schema = mongoose.Schema;

    var User = new Schema({
        username: {
            type: String,
            unique: true,
            lowercase: true
        },
        password: {
            type: String,

        },
        tfmuser: {
            type: String,
            unique: true,
            lowercase: true
        },
        isValid: {
            type: Boolean,
            default: false
        },
        isAdmin: {
            type: Boolean,
            default: false
        }
    });

    /**
     * Encrypting the user password.
     */
    User.pre('save', function(next) {
        var user = this;
        if (!user.isModified('password')) return next();
        bcrypt.hash(user.password, null, null, function(err, hash) {
            if (err) return next(err);
            user.password = hash;
            next();
        });
    });

    User.methods.comparePassword = function(password) {
        var user = this;
        return bcrypt.compareSync(password, user.password);
    };

    return mongoose.model('User', User);
};