var // Modulos
    express = require("express"),
    bodyParser = require("body-parser"),
    mongoose = require("mongoose"),
    load = require("express-load"),
    session = require("express-session"),
    flash = require("express-flash"),
    erros = require("./middleware/erros"),
    app = express();
    
app.set("view engine", "ejs");
app.set("views", __dirname + "/views");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/public"));

app.use(session({resave: true, saveUninitialized: true, secret: 'tfmtools'}));
app.use(function(req,res,next){
    res.locals.session = req.session.user_id;
    res.locals.admin = req.session.isAdmin;
    next();
});
app.use(flash());

// Conectando a database
mongoose.connect("MONGO DATABASE HERE", function(error) {
    if (error) {
        console.log(error);
    }else{
        console.log("Servidor está conectando a Database");
    }
});

load("models").then("controllers").then("routes").into(app);

//middleware
app.use(erros.notfound);
app.use(erros.serverError);

app.listen(process.env.PORT, function() {
    console.log("Servidor rodando na porta: " + process.env.PORT);
});